<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Styles -->
    <link rel="stylesheet" href="{{asset('css/app.css')}}" >
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

</head>
<body>
<div class="container">
    <h2 class="text-center">Outbound Shipping</h2>
    <form action="/" method="post">
        @csrf
        <div class="row">
            <div class="leftSide col-lg-7">
                <div class="form-group form-inline">
                    <label for="order-s-date" class="col-md-3">Date</label>
                    <input type="date" class="form-control col-md-3" name="order[s_date]" id="order-s-date">
                    <label for="order-s-date" class="col-md-3">Ship Record</label>
                    <input type="text" class="form-control col-md-3" name="order[status]" id="order-status">
                </div>
                <div class="form-group form-inline">
                    <label for="order-nbr" class="col-md-3">Order Nbr</label>
                    <input type="text" class="form-control col-md-3" name="order[rec]" id="order-nbr">
                    <label for="customer-rec" class="col-md-3">Customer Nbr</label>
                    <input type="text" class="form-control col-md-3" name="customer[rec]" id="customer-rec">
                </div>
                <div class="form-group form-inline">
                    <label for="customer-name" class="col-md-3">Name</label>
                    <input type="text" class="form-control col-md-9" name="customer[name]" id="customer-name">
                </div>
                <div class="form-group form-inline">
                    <label for="customer-company" class="col-md-3">Company</label>
                    <input type="text" class="form-control col-md-9" name="customer[company]" id="customer-company">
                </div>
                <div class="form-group form-inline">
                    <label for="customer-address" class="col-md-3">Address</label>
                    <input type="text" class="form-control col-md-9" name="customer[address]" id="customer-address">
                </div>
                <div class="form-group form-inline">
                    <div class="col-md-3"></div>
                    <input type="text" class="form-control col-md-3" name="customer[city]" id="customer-city">
                    <input type="text" class="form-control col-md-3" name="customer[state]" id="customer-state">
                    <input type="number" class="form-control col-md-3" name="customer[postal_code]" id="customer-postal-code">
                </div>
                <div class="form-group form-inline">
                    <div class="col-md-3"></div>
                    <input type="text" class="form-control col-md-4" name="customer[country]" id="customer-country">
                    <label for="customer-phone" class="col-md-2">Phone</label>
                    <input type="text" class="form-control col-md-3" name="customer[phone]" id="customer-phone">
                </div>
                <div class="form-group form-inline">
                    <label for="customer-email" class="col-md-3">Email</label>
                    <input type="text" class="form-control col-md-9" name="customer[email]" id="customer-email">
                </div>
                <div class="form-group form-inline">
                    <label class="col-md-3">Shipping standards</label>
                    <label for="order-packing-type" class="col-md-3">PackingType:</label>
                    <input type="text" class="form-control col-md-6" name="order[packing_type]" id="order-packing-type">
                </div>
                <div class="form-group form-inline">
                    <div class="col-md-3"></div>
                    <input type="text" class="form-control col-md-1" name="order[cushion]" id="order-cushion-type">
                    <label class="col-md-4">Inch cushion on all sides</label>
                </div>
                <fieldset class="scheduler-border">
                    <legend class="scheduler-border">Customer Shipping Preferences</legend>
                    <div class="form-group form-inline">
                        <label for="customer-carrier" class="col-md-3">Carrier</label>
                        <input type="text" class="form-control col-md-4" name="customer[shipping_acct_carrier]" id="customer-carrier">
                        <label for="customer-account" class="col-md-2">Account</label>
                        <input type="text" class="form-control col-md-3" name="customer[shipping_acct_nbr]" id="customer-account">
                    </div>
                    <div class="form-group form-inline">
                        <label for="customer-service" class="col-md-3">Service Level</label>
                        <input type="text" class="form-control col-md-6" name="customer[preferred_shipping_service]" id="customer-service">
                    </div>
                </fieldset>
                <div class="form-group form-inline">
                    <label for="order-carrier" class="col-md-3">Carrier</label>
                    <input type="text" class="form-control col-md-8" name="order[carrier]" id="order-carrier">
                </div>
                <div class="form-group form-inline">
                    <label for="order-tracking" class="col-md-3">Tracking</label>
                    <input type="text" class="form-control col-md-8" name="order[tracking]" id="order-tracking">
                </div>
                <div class="form-group form-inline">
                    <label for="order-ship-to" class="col-md-3">Alternate Ship To</label>
                    <textarea rows="2" class="form-control col-md-8" name="order[ship_to]" id="order-ship-to"></textarea>
                </div>
            </div>
            <div class="rightSide col-lg-5">
                <div class="form-group form-inline">
                    <label for="order-rma" class="col-md-4">RMA</label>
                    <input type="text" class="form-control col-md-5" name="order[rma]" id="order-rma">
                </div>
                <h6 style="text-align: center">Box Codes</h6>
                <div class="form-group form-inline">
                    <label for="customer-box-code" class="col-md-4">Recommended</label>
                    <input type="text" class="form-control col-md-8" name="model[box_code]" id="customer-box-code">
                </div>
                <div class="form-group form-inline">
                    <label for="customer-box-code2" class="col-md-4">Alternative</label>
                    <input type="text" class="form-control col-md-8" name="model[box_code2]" id="customer-box-code2">
                </div>
                <div class="form-group form-inline">
                    <label for="customer-box-code2" class="col-md-4"></label>
                    <label for="customer-box-code2" class="col-md-4">Box 1</label>
                    <label for="customer-box-code2" class="col-md-4">Box 2</label>
                </div>
                <div class="form-group form-inline">
                    <label for="shipping-box-code" class="col-md-4">Box used</label>
                    <input type="text" class="form-control col-md-4" name="box[0][box_code]" id="shipping-box-code">
                    <input type="text" class="form-control col-md-4" name="box[1][box_code]" id="shipping-box-code2">
                </div>
                <div class="form-group form-inline">
                    <label for="shipping-box-code" class="col-md-4">dim1</label>
                    <input type="text" class="form-control col-md-4" name="box[0][dim1]" id="shipping-box-code">
                    <input type="text" class="form-control col-md-4" name="box[1][dim1]" id="shipping-box-code-2">
                </div>
                <div class="form-group form-inline">
                    <label for="shipping-box-code" class="col-md-4">dim2</label>
                    <input type="text" class="form-control col-md-4" name="box[0][dim2]" id="shipping-box-code">
                    <input type="text" class="form-control col-md-4" name="box[1][dim2]" id="shipping-box-code-2">
                </div>
                <div class="form-group form-inline">
                    <label for="shipping-box-code" class="col-md-4">dim3</label>
                    <input type="text" class="form-control col-md-4" name="box[0][dim3]" id="shipping-box-code">
                    <input type="text" class="form-control col-md-4" name="box[1][dim3]" id="shipping-box-code-2">
                </div>
                <div class="form-group form-inline">
                    <label for="shipping-box-code" class="col-md-4">Inventory</label>
                    <input type="text" class="form-control col-md-4" name="box[0][in_stock]" id="shipping-box-code">
                    <input type="text" class="form-control col-md-4" name="box[1][in_stock]" id="shipping-box-code-2">
                </div>
                <div class="form-group form-inline">
                    <h4>Actual Shipping Dimensions</h4>
                </div>
                <div class="form-group form-inline">
                    <label for="shipping-box-code" class="col-md-4">dim1</label>
                    <input type="text" class="form-control col-md-4" name="order[dim1]" id="order-dim1">
                    <input type="text" class="form-control col-md-4" name="order[dim1A]" id="order-dim1A">
                </div>
                <div class="form-group form-inline">
                    <label for="shipping-box-code" class="col-md-4">dim2</label>
                    <input type="text" class="form-control col-md-4" name="order[dim2]" id="order-dim2">
                    <input type="text" class="form-control col-md-4" name="order[dim2A]" id="order-dim2A">
                </div>
                <div class="form-group form-inline">
                    <label for="shipping-box-code" class="col-md-4">dim3</label>
                    <input type="text" class="form-control col-md-4" name="order[dim3]" id="order-dim3">
                    <input type="text" class="form-control col-md-4" name="order[dim3A]" id="order-dim3A">
                </div>
                <div class="form-group form-inline">
                    <label for="shipping-box-code" class="col-md-4">Weight</label>
                    <input type="text" class="form-control col-md-3" name="order[weight]" id="order-weight">
                    <input type="text" class="form-control col-md-3" name="order[weight2]" id="order-weight2">
                    <label class="col-md-2">lbs</label>
                </div>
                <div class="form-group form-inline">
                    <label for="order-ship-cost" class="col-md-4">Shipping Cost</label>
                    <input type="text" class="form-control col-md-5" name="order[ship_cost]" id="order-ship-cost">
                </div>
                <div class="form-group form-inline">
                    <label for="order-shipping" class="col-md-4">Shipping Billed</label>
                    <input type="text" class="form-control col-md-5" name="order[shipping]" id="order-shipping">
                </div>
                <div class="form-group form-inline">
                    <label for="order-bill-to" class="col-md-4">BillTo</label>
                    <input type="text" class="form-control col-md-1" name="order[bill_to]" id="order-bill-to">
                </div>
                <div class="form-group form-inline">
                    <label for="order-res-del-flag" class="col-md-4">ResDelFlg</label>
                    <input type="text" class="form-control col-md-1" name="order[res_del_flag]" id="order-res-del-flag">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group form-inline">
                    <label for="order-comment" class="col-md-1">Comment</label>
                    <textarea rows="2" class="form-control col-md-10" name="order[comment]" id="order-comment"></textarea>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group form-inline">
                    <label for="order-items-listing-nbr" class="col-md-1">Listing</label>
                    <label for="order-items-stk-nbr" class="col-md-1">SN</label>
                    <label for="inventory-badge" class="col-md-1">Badge</label>
                    <label for="inventory-model" class="col-md-1">Model</label>
                    <label for="inventory-serial-nbr" class="col-md-1">Serial Nbr</label>
                    <label for="order-items-qty" class="col-md-1"></label>
                    <label for="model-weight" class="col-md-1">weight</label>
                    <label for="model-box-code" class="col-md-1">Box Code</label>
                    <label for="customer-service" class="col-md-1">Warranty Seal</label>
                </div>
                <div class="form-group form-inline">
                    <input type="text" class="form-control col-md-1" name="order_items[listing_nbr]" id="order-items-listing-nbr">
                    <input type="text" class="form-control col-md-1" name="order_items[stk_nbr]" id="order-items-stk-nbr">
                    <input type="text" class="form-control col-md-1" name="inventory[badge]" id="inventory-badge">
                    <input type="text" class="form-control col-md-1" name="inventory[model]" id="inventory-model">
                    <input type="text" class="form-control col-md-1" name="inventory[serial_nbr]" id="inventory-serial-nbr">
                    <input type="text" class="form-control col-md-1" name="order_items[qty]" id="order-items-qty">
                    <input type="text" class="form-control col-md-1" name="model[weight]" id="model-weight">
                    <input type="text" class="form-control col-md-1" name="model[box_code]" id="model-box-code">
                    <input type="text" class="form-control col-md-1" name="order_items[seal]" id="order-items-seal">
                    <input type="text" class="form-control col-md-1" name="order_items[price]" id="order-items-price">
                </div>
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>

</body>
</html>
