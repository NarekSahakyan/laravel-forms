<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as BaseModel;

class Model extends BaseModel
{
    protected $fillable = [
        'box_code',
        'box_code2',
        'rec_num',
        'weight',
    ];
}
