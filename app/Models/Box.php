<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Box extends Model
{
    //

    protected $fillable = [
        'box_code',
        'in_stock',
        'dim1',
        'dim2',
        'dim3',
    ];
}
