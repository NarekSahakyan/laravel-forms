<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{
    protected $fillable = [
        'mod_rec',
        'stock_nbr',
        'badge',
        'model',
        'serial_nbr',
    ];
}
