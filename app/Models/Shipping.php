<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Shipping extends Model
{
    protected $fillable = [
        'rec',
        'order_nbr',
        'box_code',
        'box_code2',
    ];
}
