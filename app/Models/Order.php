<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'rec',
        'cust_nbr',
        'shipping',
        'rma',
        'dim1',
        'dim1A',
        'dim2',
        'dim2A',
        'dim3',
        'dim3A',
        'carrier',
        'tracking',
        'ship_to',
        'ship_cost',
        'comment',
        'bill_to',
        'res_del_flag',
        'weight',
        'weight2',
        's_date',
        'packing_type',
        'cushion',
    ];
}
