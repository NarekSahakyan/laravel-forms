<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    protected $fillable = [
        'stk_nbr',
        'order_nbr',
        'listing_nbr',
        'qty',
        'seal',
        'price',
    ];
}
