<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = [
        'rec',
        'name',
        'company',
        'address',
        'city',
        'state',
        'country',
        'postal_code',
        'phone',
        'email',
        'shipping_acct_carrier',
        'shipping_acct_nbr',
        'preferred_shipping_service',
    ];
}
