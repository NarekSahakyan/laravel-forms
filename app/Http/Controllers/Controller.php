<?php

namespace App\Http\Controllers;

use App\Models\Box;
use App\Models\Customer;
use App\Models\Inventory;
use App\Models\Model;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Shipping;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function post(Request $request)
    {
        Order::create(array_merge($request->post('order', []), [
            'cust_nbr' =>  $request->post('customer')['rec']
        ]));

        Customer::create($request->post('customer'));
        Model::create($request->post('model'));
        Inventory::create($request->post('inventory'));

        Shipping::create(array_merge($request->post('shipping', []), [
            'order_nbr' => $request->post('order')['rec'],
            'box_code' => $request->post('box')[0]['box_code'],
            'box_code2' => $request->post('box')[1]['box_code'],
        ]));

        OrderItem::create(array_merge($request->post('order_items', []), [
            'order_nbr' => $request->post('order')['rec']
        ]));

        foreach ($request->post('box') as $boxData) {
            Box::create(array_merge($boxData));
        };

        return redirect()->back();
    }
}
