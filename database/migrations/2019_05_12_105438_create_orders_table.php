<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('rec')->nullable();
            $table->integer('cust_nbr')->nullable();
            $table->string('shipping')->nullable();
            $table->string('rma')->nullable();
            $table->float('dim1')->nullable();
            $table->float('dim1A')->nullable();
            $table->float('dim2')->nullable();
            $table->float('dim2A')->nullable();
            $table->float('dim3')->nullable();
            $table->float('dim3A')->nullable();
            $table->string('carrier')->nullable();
            $table->string('tracking')->nullable();
            $table->text('ship_to')->nullable();
            $table->float('ship_cost')->nullable();
            $table->text('comment')->nullable();
            $table->string('bill_to')->nullable();
            $table->string('res_del_flag')->nullable();
            $table->float('weight')->nullable();
            $table->float('weight2')->nullable();
            $table->date('s_date')->nullable();
            $table->string('packing_type')->nullable();
            $table->string('cushion')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
